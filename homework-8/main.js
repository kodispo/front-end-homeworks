const obj = {
    name: 'Alina',
    age: 23,
    address: {
        country: 'UA',
        city: 'Kyiv'
    },
    family: {
        father: {
            name: 'Oleg',
            age: 47,
            address: {
                country: 'UA',
                city: 'Lviv'
            },
        },
        mother: {
            name: 'Anna',
            age: 43,
            address: {
                country: 'UA',
                city: 'Odessa'
            }
        }
    }
}

const clone = copy(obj);
console.log(clone);

function copy(obj) {
    const clone = {};
    for (let prop in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, prop)) {
            if (typeof obj[prop] === 'object' && obj[prop] !== null) {
                clone[prop] = copy(obj[prop]);
            } else {
                clone[prop] = obj[prop];
            }
        }
    }
    return clone;
}

/**
 * Note:
 * I read that Object.prototype.hasOwnProperty.call(obj, prop) is safer.
 * obj.hasOwnProperty(prop) will fail if the object has an unrelated field with the same name.
 * E.g.:
 * var obj = { foo: 42, hasOwnProperty: 'lol' };
 * obj.hasOwnProperty('foo'); // TypeError: hasOwnProperty is not a function
 */
