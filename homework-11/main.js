const formElem     = document.getElementById('todo-form');
const seedElem     = document.getElementById('seed-todo');
const clearAllElem = document.getElementById('clear-all');

initToDo();

formElem.addEventListener('submit', handleToDoFormSubmit);
document.addEventListener('click',  handleTaskClick);
document.addEventListener('click',  handleCloseTaskButtonClick);
seedElem.addEventListener('click',  handleSeederClick);
clearAllElem.addEventListener('click', handleClearAllClick);


function handleToDoFormSubmit(event) {
    event.preventDefault();

    const inputElem = document.getElementById('new-task');
    const taskText  = inputElem.value;
    if (!taskText) return;

    const taskElem = createTaskElement(taskText);
    addToList(taskElem);

    inputElem.value = '';
    inputElem.focus();
}

function handleTaskClick(event) {
    const element = event.target;

    if (element.closest('.task') && !element.classList.contains('close')) {
        toggleTask(element.closest('.task'));
    }
}

function handleCloseTaskButtonClick(event) {
    const element = event.target;

    if (element.classList.contains('close')) {
        removeTask(element.closest('.task'));
    }
}

function handleSeederClick(event) {
    event.preventDefault();

    const seedsArr = [
        {
            text: 'Create todo list',
            completed: true
        },
        {
            text: 'Feed the dog',
            completed: true
        },
        {
            text: 'Clean up the apartment',
            completed: false
        },
        {
            text: 'Complete daily workout',
            completed: false
        },
        {
            text: 'Visit a dentist',
            completed: false
        }
    ];

    populateTasks(seedsArr);
    updateStorageTasks();
    toggleClearAllElem();
}

function handleClearAllClick(event) {
    event.preventDefault();

    document.getElementById('todo-list').innerHTML = '';
    updateStorageTasks();
    toggleClearAllElem();
}


function createTaskElement(text) {
    const taskElem = document.createElement('li');
    const taskText = document.createElement('span');
    const closeBtn = document.createElement('button');

    taskElem.classList   = 'task list-group-item list-group-item-action list-group-item-warning';
    taskText.textContent = text;
    closeBtn.classList   = 'close text-danger';
    closeBtn.innerHTML   = '&times;';
    closeBtn.setAttribute('type', 'button');

    taskElem.append(taskText);
    taskElem.append(closeBtn);

    return taskElem;
}

function addToList(taskElem, updateStorage = true) {
    document.getElementById('todo-list').append(taskElem);
    if (updateStorage) updateStorageTasks();
    toggleClearAllElem();
}

function toggleTask(taskElem) {
    if (taskElem.classList.contains('list-group-item-warning')) {
        setTaskCompleted(taskElem);
    } else {
        setTaskOpened(taskElem);
    }
    updateStorageTasks();
}

function setTaskOpened(taskElem) {
    taskElem.classList.remove('list-group-item-success');
    taskElem.classList.add('list-group-item-warning');
}

function setTaskCompleted(taskElem) {
    taskElem.classList.remove('list-group-item-warning');
    taskElem.classList.add('list-group-item-success');
}

function removeTask(taskElem) {
    taskElem.remove();
    updateStorageTasks();
    toggleClearAllElem();
}


function getTasksData() {
    const tasksArr = [];

    for (const taskElem of document.querySelectorAll('.task')) {
        const taskObj = {};

        taskObj.text      = taskElem.querySelector('span').textContent;
        taskObj.completed = taskElem.classList.contains('list-group-item-success');

        tasksArr.push(taskObj);
    }

    return tasksArr;
}

function updateStorageTasks() {
    localStorage.setItem('tasks', JSON.stringify(getTasksData()));
}

function getStorageTasks() {
    return JSON.parse(localStorage.getItem('tasks'));
}

function populateTasks(tasksArr) {
    tasksArr.forEach(function (taskObj) {
        const taskElem = createTaskElement(taskObj.text);
        if (taskObj.completed) setTaskCompleted(taskElem);
        addToList(taskElem, false);
    });
}

function toggleClearAllElem() {
    const tasksCount   = document.querySelectorAll('.task').length;
    const clearAllElem = document.getElementById('clear-all');

    clearAllElem.style.display = tasksCount ? 'block' : 'none';
}

function initToDo() {
    const tasksArr = getStorageTasks();
    if (tasksArr) populateTasks(tasksArr);
    toggleClearAllElem();
}