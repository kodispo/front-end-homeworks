const WEATHER_API_KEY = 'b961a96b1f6e80d9ec8a66f3b9670468';
const GEO_API_KEY     = '0c84da45171d4450a232a02a8c5e7e05';

const formEl    = document.getElementById('get-weather-form');
const inputEl   = document.getElementById('city');
const infoEl    = document.getElementById('info');
const alertEl   = document.querySelector('.alert');
const weatheUrl = new URL('https://api.openweathermap.org/data/2.5/weather');
const geoUrl    = new URL('https://ipgeolocation.abstractapi.com/v1');

/**
 * Weather API
 */
formEl.addEventListener('submit', handleFormSubmit);

function handleFormSubmit(e) {
    e.preventDefault();
    alertEl.style.display = 'none';

    if (!inputEl.value) {
        alertEl.textContent = 'Please specify a city.';
        alertEl.style.display = 'block';
        return;
    }

    showWeather(inputEl.value);
}

function showWeather(city) {
    infoEl.innerHTML = '';

    weatheUrl.search = new URLSearchParams({
        q: city,
        appid: WEATHER_API_KEY,
        units: 'metric',
    });
    
    fetch(weatheUrl)
        .then(response => {
            if (!response.ok) throw new Error(response.statusText);
            return response.json();
        })
        .then(data => {
            const infoObj = {
                'city': `${data.name}, ${data.sys.country}`,
                'weather': data.weather[0].description,
                'temperature': `${Math.round(data.main.temp)} °C`,
                'humidity': `${data.main.humidity} %`,
                'pressure': `${data.main.pressure} hPa`,
                'wind': `${data.wind.speed} m/s`,
            }

            for (const [key, value] of Object.entries(infoObj)) {
                const liEl = document.createElement('li');
                liEl.classList = 'list-group-item';
                liEl.innerHTML = `<strong>${key.charAt(0).toUpperCase() + key.slice(1)}:</strong> ${value}`;
                infoEl.append(liEl);
            }
        })
        .catch(err => {
            alertEl.textContent = err;
            alertEl.style.display = 'block';
        });
}


/**
 * Geo API
 */
if (!localStorage.getItem('city')) {
    geoUrl.search = new URLSearchParams({
        api_key: GEO_API_KEY,
    });
    
    fetch(geoUrl)
        .then(response => response.json())
        .then(data => {
            if (!data || !data.city) return;
            localStorage.setItem('city', data.city);
            showHint();
        })
        .catch(err => console.err(err));
} else {
    showHint();
}

function showHint() {
    const hintEl = document.createElement('div');
    hintEl.setAttribute('id', 'hint');
    hintEl.innerHTML = `Are you located in <a href="#">${localStorage.getItem('city')}</a>?`;
    formEl.after(hintEl);
}

document.querySelector('.wrap').addEventListener('click', handleHintClick);

function handleHintClick(e) {
    if (e.target.closest('#hint a')) {
        e.preventDefault();
        showWeather(localStorage.getItem('city'));
    }
}