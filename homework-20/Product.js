class Product {
    constructor(name, price, qty = 1) {
        this.id = this.createId();
        if (name) {
            this.name = name;
        } else {
            throw 'Product name must not be empty';
        }
        if (+price > 0) {
            this.price = price;
        } else {
            throw 'Price must be greater than 0';
        }
        if (+qty > 0) {
            this.qty = qty;
        } else {
            throw 'Quantity must be greater than 0';
        }
    }

    createId() {
        return Math.random().toString(36).substr(2, 9);
    }
}