const formEl  = document.getElementById('new-product-form');
const alertEl = document.querySelector('.alert');
const cartEl  = document.getElementById('cart');
const totalEl = document.getElementById('total');

// seeds
const seeds = [
    new Product('iPhone', 1300, 2),
    new Product('iWatch', 400, 3),
];
const cart = new Cart(seeds);
cart.products.forEach((product) => {
    const row = cart.createProductRowEl(product);
    cartEl.append(row);
});
totalEl.textContent = cart.getTotalPrice();


formEl.addEventListener('submit', handleFormSubmit);

function handleFormSubmit(e) {
    e.preventDefault();

    alertEl.style.display = 'none';

    const nameEl  = formEl.querySelector('#new-name');
    const qtyEl   = formEl.querySelector('#new-qty');
    const priceEl = formEl.querySelector('#new-price');

    const name  = nameEl.value;
    const qty   = qtyEl.value;
    const price = priceEl.value;

    if (!name || +qty < 1 || +price < 1) {
        alertEl.style.display = 'block';
        return;
    }

    const product = new Product(name, price, qty);
    cart.addProduct(product);

    const row = cart.createProductRowEl(product);
    cartEl.append(row);
    totalEl.textContent = cart.getTotalPrice();

    nameEl.value  = '';
    qtyEl.value   = '';
    priceEl.value = '';
}

cartEl.addEventListener('click', handleRemoveBtnClick);

function handleRemoveBtnClick(e) {
    if (!e.target.classList.contains('close')) return;

    const productEl = e.target.closest('.product');
    const productId = productEl.getAttribute('id');

    cart.removeProduct(productId);
    productEl.remove();
    totalEl.textContent = cart.getTotalPrice();
}