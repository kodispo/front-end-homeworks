class Cart {
    products = [];

    constructor(productsArr) {
        if (!productsArr) return;
        productsArr.forEach(product => this.addProduct(product));
    }

    addProduct(product) {
        if (product instanceof Product) {
            this.products.push(product);
        } else {
            throw 'Not a Product instance is tried to be stored in the Cart';
        }
    }

    removeProduct(productId) {
        this.products = this.products.filter((product) => product.id !== productId);
    }

    getTotalPrice() {
        return this.products.reduce((sum, product) => sum + product.price * product.qty, 0)
    }

    getProductRowHtml(product) {
        return `
            <tr id="${product.id}" class="product">
                <td>${product.name}</td>
                <td>${product.qty}</td>
                <td>${product.price}</td>
                <td><button class="close text-danger" type="button">×</button></td>
            </tr> 
        `;
    };

    convertHtmlToEl(html) {
        let template = document.createElement('template');
        template.innerHTML = html.trim();
        return template.content.firstChild;
    };

    createProductRowEl(product) {
        const html = this.getProductRowHtml(product);
        return this.convertHtmlToEl(html);
    }
}