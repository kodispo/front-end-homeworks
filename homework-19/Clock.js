class Clock {
    getFormattedTime() {
        const dateObj = new Date();
        const hh      = this.convertToDoubleDigits(dateObj.getHours());
        const mm      = this.convertToDoubleDigits(dateObj.getMinutes());
        const ss      = this.convertToDoubleDigits(dateObj.getSeconds());

        return `${hh}:${mm}:${ss}`;
    }

    getFormattedDate() {
        const dateObj = new Date();
        const dd      = this.convertToDoubleDigits(dateObj.getDate());
        const mm      = this.convertToDoubleDigits(dateObj.getMonth() + 1);
        const yyyy    = dateObj.getFullYear();

        return `${dd}:${mm}:${yyyy}`;
    }

    convertToDoubleDigits(num) {
        return (num < 10) ? '0' + num : num;
    }

    updateDOMElement(element) {
        element.querySelector('#time').textContent = this.getFormattedTime();
        element.querySelector('#date').textContent = this.getFormattedDate();
    }

    run(element) {
        setInterval(this.updateDOMElement.bind(this), 300, element);
    }
}