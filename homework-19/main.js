const clock     = new Clock();
const wrapElem  = document.querySelector('.wrap');
const clockElem = document.querySelector('#clock');

VanillaTilt.init(wrapElem, {
    max: 25,
    speed: 400,
    perspective: 2000,
});

clock.run(clockElem);