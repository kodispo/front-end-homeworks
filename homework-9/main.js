const revenueElements = document.querySelectorAll('[data-id="revenue-value"]');
const sumElement      = document.getElementById('sum');

let total = 0;

Array.from(revenueElements).forEach(el => total += parseAmount(el.textContent));
sumElement.textContent = `${sumElement.textContent} ${shortenAmount(total)}`;


function parseAmount(amount) {
    const match = amount.match(/(.+)(M|B)$/);
    if (!match) return +amount;

    const number   = +match[1];
    const shortcut = match[2];

    switch(shortcut) {
        case 'M': return number * 1000000;
        case 'B': return number * 1000000000;
    }
}

function shortenAmount(amount) {
    if (amount >= 1000000000) return (amount/1000000000).toFixed(1) + 'B';
    if (amount >= 1000000)    return (amount/1000000).toFixed(1) + 'M';
    return amount;
}