const url   = 'https://jsonplaceholder.typicode.com/todos';
const count = 10;

document.addEventListener('DOMContentLoaded', handleDOMContentLoad);

function handleDOMContentLoad() {
    fetch(url)
        .then(response => response.json())
        .then(todos => {
            for (let todo of todos.slice(0 , count)) {
                const listEl = document.getElementById('todo-list');
                const todoEl = createTodoEl(todo);
                listEl.append(todoEl);
            }
        })
        .catch(err => console.error(err));
}

function createTodoEl(todoObj) {
    const todoEl = document.createElement('li');
    todoEl.setAttribute('data-id', todoObj.id);
    todoEl.classList = 'task list-group-item';
    todoEl.classList.add(todoObj.completed ? 'list-group-item-success' : 'list-group-item-warning');
    todoEl.innerHTML = `<span>${todoObj.id}. ${todoObj.title}</span>`;
    return todoEl;
}