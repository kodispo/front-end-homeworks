"use strict";

class Calculator {
    #theNum; // Current number
    #oldNum; // First number

    viewer;    // Calculator screen where result is displayed
    equals;    // Equal button
    nums;      // List of numbers
    ops;       // List of operators
    resultNum; // Result
    operator;  // Batman

    constructor() {
        this.#theNum = "";
        this.#oldNum = "";
    
        this.viewer = Calculator.el("#viewer");
        this.equals = Calculator.el("#equals");
        this.nums   = Calculator.el(".num");
        this.ops    = Calculator.el(".ops");

        /* The click events */
        this.nums.forEach(el => el.addEventListener('click', this.setNum.bind(this)));
        this.ops.forEach(el => el.addEventListener('click', this.moveNum.bind(this)));
        this.equals.addEventListener('click', this.displayNum.bind(this))
        Calculator.el("#clear").addEventListener('click', this.clearAll.bind(this));
        Calculator.el("#reset").addEventListener('click', () => window.location = window.location);
    }

    // Shortcut to get elements
    static el(el) {
        if (el.charAt(0) === "#") return document.querySelector(el);
        return document.querySelectorAll(el);
    }

    // When: Number is clicked. Get the current number selected
    setNum(e) {
        if (this.resultNum) { // If a result was displayed, reset number
            this.#theNum = e.target.getAttribute("data-num");
            this.resultNum = "";
        } else { // Otherwise, add digit to previous number (this is a string!)
            this.#theNum += e.target.getAttribute("data-num");
        }
        this.viewer.innerHTML = this.#theNum; // Display current number
    }

    // When: Operator is clicked. Pass number to oldNum and save operator
    moveNum(e) {
        this.#oldNum = this.#theNum;
        this.#theNum = "";
        this.operator = e.target.getAttribute("data-ops");
        this.equals.setAttribute("data-result", ""); // Reset result in attr
    }

    // When: Equals is clicked. Calculate result
    displayNum() {
        // Convert string input to numbers
        this.#oldNum = parseFloat(this.#oldNum);
        this.#theNum = parseFloat(this.#theNum);
    
        // Perform operation
        switch (this.operator) {
          case "plus": this.resultNum = this.#oldNum + this.#theNum; break;
          case "minus": this.resultNum = this.#oldNum - this.#theNum; break;
          case "times": this.resultNum = this.#oldNum * this.#theNum; break;
          case "divided by": this.resultNum = this.#oldNum / this.#theNum; break;
          default: this.resultNum = this.#theNum;
        }
    
        // If NaN or Infinity returned
        if (!isFinite(this.resultNum)) {
          if (isNaN(this.resultNum)) { // If result is not a number; set off by, eg, double-clicking operators
            this.resultNum = "You broke it!";
          } else { // If result is infinity, set off by dividing by zero
            this.resultNum = "Look at what you've done";
            Calculator.el('#calculator').classList.add("broken"); // Break calculator
            Calculator.el('#reset').classList.add("show"); // And show reset button
          }
        }
    
        // Display result, finally!
        this.viewer.innerHTML = this.resultNum;
        this.equals.setAttribute("data-result", this.resultNum);
    
        // Now reset this.#oldNum & keep result
        this.#oldNum = 0;
        this.#theNum = this.resultNum;
    }

    // When: Clear button is pressed. Clear everything
    clearAll() {
        this.#oldNum = "";
        this.#theNum = "";
        this.viewer.innerHTML = "0";
        this.equals.setAttribute("data-result", this.resultNum);
    }
}

const calculator = new Calculator();