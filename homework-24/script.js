const TILES = [];
const countEl = document.getElementById('count');

let count;

function initGame() {
  const tilesContainer = document.getElementById("tiles");
  tilesContainer.innerHTML = "";
  tilesContainer.addEventListener("click", onTileClick);
  TILES.length = 0; // clear all tiles in the array
  let shuffledEls = getTiles().concat(0).map(el => createTileEl(el));
  while (shuffledEls.length) TILES.push(shuffledEls.splice(0,4));
  renderTiles();
  count = initCounter(countEl);
}

function shuffleTiles() {
  return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].sort(() => Math.random()-0.5);
}

function isSolvable(arr) {
  let count = 0;
  for (let i = 0; i < arr.length - 1; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[i]) count++;
    }
  }
  return count % 2 === 0;
}

function getTiles() {
  let tilesOrder = shuffleTiles();
  if (!isSolvable(tilesOrder)) getTiles();
  return tilesOrder;
}

function isVictory() {
  let tiles = [...TILES.flat()];
  let victory = true;
  for (let i = 0; i < tiles.length - 1; i++) {
    if ((i + 1) !== +tiles[i].textContent) {
      victory = false;
      break;
    }
  }
  return victory;
}

function createTileEl(text) {
  const tile = document.createElement("div");
  tile.classList.add("tile");
  tile.textContent = text !== 0 ? text : '';
  return tile;
}

function renderTiles() {
  const tilesContainer = document.getElementById("tiles");
  tilesContainer.innerHTML = "";
  for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 4; j++) {
      tilesContainer.append(TILES[i][j]);
    }
  }
}

function onTileClick(e) {
  if (e.target.classList.contains("tile")) {
    const id = +e.target.textContent;
    if (id) {
      swapTiles(id);
    }
  }
  renderTiles();

  if (isVictory()) {
    const tilesContainer = document.getElementById("tiles");
    tilesContainer.innerHTML = `Victory! Your score is: ${countEl.textContent} steps.`;
  }
}

function swapTiles(id) {
  const [tileX, tileY] = findTileCoordById(id);
  const [emptyX, emptyY] = findEmptyTileCoords();

  // are they neighbors
  if (
    (tileX === emptyX && Math.abs(tileY - emptyY) === 1) ||
    (tileY === emptyY && Math.abs(tileX - emptyX) === 1)
  ) {
    const temp = TILES[tileX][tileY];
    TILES[tileX][tileY] = TILES[emptyX][emptyY];
    TILES[emptyX][emptyY] = temp;
    count();
  }
}

function findEmptyTileCoords() {
  for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 4; j++) {
      if (TILES[i][j].textContent === "") {
        return [i, j];
      }
    }
  }
}

function findTileCoordById(id) {
  for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 4; j++) {
      if (TILES[i][j].textContent == id) {
        return [i, j];
      }
    }
  }
}

function initCounter(el, counter = 0) {
  el.textContent = counter;
  return () => el.textContent = ++counter;
}