const number = requestNumber();
const row    = getFactorialOperandsRow(number);
const sum    = sumFactorialOperandsRow(row);

print(row);
print(sum, 'The sum of numbers:');


function requestNumber() {
    let number = prompt('Please enter natural number:');
    if (!isNaturalNumber(number)) {
        alert('Validation error! Please try again.');
        number = requestNumber();
    }
    return +number;
}

function isNaturalNumber(n) {
    return !Number.isNaN(+n) && n > 0 && Math.floor(n) === +n;
}

function getFactorialOperandsRow(number) {
    if (number === 1) return '1';
    return getFactorialOperandsRow(number - 1) + ' ' + number;
}

function sumFactorialOperandsRow(row) {
    if (!Array.isArray(row)) row = row.split(' ').map(n => +n);
    if (row.length === 1) return row[0];
    return row.pop() + sumFactorialOperandsRow(row);
}

function print(content, prefix) {
    content = (prefix) ? prefix + ' ' + content : content;
    console.log(content);
}