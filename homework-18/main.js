const loginFormEl = document.getElementById('login-form');
const userCardEl  = document.getElementById('user-card');
const userNameEl  = document.getElementById('user-name');
const logoutEl    = document.getElementById('logout');
const user        = new User();

if (user.isLoggedIn()) {
    userNameEl.textContent = user.name;
    userCardEl.style.display = 'block';
} else {
    loginFormEl.style.display = 'block';
}

loginFormEl.addEventListener('submit', handleLoginFormSubmit);
logoutEl.addEventListener('click', handleLogoutClick);

function handleLoginFormSubmit(e) {
    e.preventDefault();
    const nameEl  = loginFormEl.querySelector('#name');
    const alertEl = loginFormEl.querySelector('.alert');
    alertEl.style.display = 'none';
    if (nameEl.value) {
        user.login(nameEl.value);
        userNameEl.textContent = user.name;
        loginFormEl.style.display = 'none';
        userCardEl.style.display = 'block';
        nameEl.value = '';
    } else {
        alertEl.style.display = 'block';
    }
}

function handleLogoutClick(e) {
    e.preventDefault();
    user.logout();
    userNameEl.textContent = '';
    userCardEl.style.display = 'none';
    loginFormEl.style.display = 'block';
}