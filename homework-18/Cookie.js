class Cookie {
    static get(name) {
        const cookiesArr = document.cookie.split('; ');
        for (let i = 0; i < cookiesArr.length; i++) {
            const keyValArr = cookiesArr[i].split('=');
            const key       = decodeURIComponent(keyValArr[0]);
            const val       = decodeURIComponent(keyValArr[1]);
            if (key === name) return val;
        }
    }

    static set(name, value, maxAge = 3600, options = {}) {
        let optionsStr = '';
        for (let prop in options) {
            if (options.hasOwnProperty(prop)) {
                optionsStr += `; ${prop}=${options[prop]}`;
            }
        }
        document.cookie = `${encodeURIComponent(name)}=${encodeURIComponent(value)}; max-age=${encodeURIComponent(maxAge)}${optionsStr}`;
    }

    static unset(name) {
        this.set(name, '', -1);
    }
}