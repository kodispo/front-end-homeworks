class User {
    constructor() {
        if (this.isLoggedIn()) {
            this.name = Cookie.get('user');
        }
    }

    get name() {
        return this._name;
    }

    set name(name) {
        if (name) this._name = name;
    }

    isLoggedIn() {
        return !!Cookie.get('user');
    }

    login(name) {
        this.name = name;
        Cookie.set('user', name);
    }

    logout() {
        Cookie.unset('user');
    }
}