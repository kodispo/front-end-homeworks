class User {
    constructor() {
        if (this.isLoggedIn()) {
            this.name = localStorage.getItem('user');
        }
    }

    get name() {
        return this._name;
    }

    set name(name) {
        if (name) this._name = name;
    }

    isLoggedIn() {
        return !!localStorage.getItem('user');
    }

    login(name) {
        this.name = name;
        localStorage.setItem('user', name);
    }

    logout() {
        localStorage.removeItem('user');
    }
}