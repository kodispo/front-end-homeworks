import { add }  from './operations/add.js';
import { sub }  from './operations/sub.js';
import { mult } from './operations/mult.js';
import { div }  from './operations/div.js';

export { add, sub, mult, div };