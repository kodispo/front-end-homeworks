const calc = createCalculator(10);

console.log(calc.sum(5));
console.log(calc.mult(10));
console.log(calc.sub(40));
console.log(calc.div(10));
console.log(calc.set(100));


function createCalculator(initial = 0) {
    let result = +initial;

    function sum(number) {
        return result += +number;
    }

    function mult(number) {
        return result *= +number;
    }

    function sub(number) {
        return result -= +number;
    }

    function div(number) {
        return result /= +number;
    }

    function set(number) {
        return result = +number;
    }

    return {
        sum,
        mult,
        sub,
        div,
        set,
    }
}