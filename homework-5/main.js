const radius = getRadius();
const operationCode = getOperationCode();

runOperation(operationCode, radius);


function getRadius() {
    let radius;

    do {
        radius = prompt('Введите радиус круга:');
        if (!radius || isNaN(+radius)) {
            alert('Ошибка! Необходимо ввести число. Пожалуйста, повторите.');
        }
    } while (!radius || isNaN(+radius))

    return +radius;
}

function getOperationCode() {
    let operationCode;

    while (![1,2,3].includes(+operationCode)) {
        operationCode = prompt('Введите номер команды чтобы посчитать:\n1 - диаметр, 2 - площадь круга, 3 - длинну окружности');
        if (![1,2,3].includes(+operationCode)) {
            alert('Ошибка! Такой команды не существует. Пожалуйста, повторите.');
        }
    }

    return +operationCode;
}

function runOperation(operationCode, radius) {
    switch (operationCode) {
        case 1: calcCircleDiameter(radius); break;
        case 2: calcCircleSquare(radius); break;
        case 3: calcCircumference(radius); break;
        default: console.error('Error! No operation was performed.');
    }
}

function calcCircleDiameter(radius) {
    const diameter = radius * 2;
    alert(`Диаметр окружности с радиусом ${radius} равен ${diameter}.`);
}

function calcCircleSquare(radius) {
    const square = Math.round(Math.PI * Math.pow(radius, 2));
    alert(`Площадь окружности с радиусом ${radius} равна ${square}.`);
}

function calcCircumference(radius) {
    const circumference = Math.round(2 * Math.PI * radius);
    alert(`Длина окружности с радиусом ${radius} равна ${circumference}.`);
}