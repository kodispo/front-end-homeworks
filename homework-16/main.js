/**
 * Задача 1
 */
const personJohn = {
    name: "John",
    sayHello: function() {
        console.log(`Hello, I'm ${this.name}`);
    }
}

const sysAdmin = {
    name: "Bob",
    __proto__: personJohn, // *
}

const clientNatalia = {
    name: "Natalia",
    __proto__: personJohn, // *
}

personJohn.sayHello();
sysAdmin.sayHello();
clientNatalia.sayHello();


/**
 * Задача 2
 */
function Person(name) {
    this.name = name;
    this.sayHello = function() {
        console.log(`Hello, I'm ${this.name}`);
    }
}

function Employee(name) {
    this.name = name;
}

function Client(name) {
    this.name = name;
}

Employee.prototype = new Person(); // *
Client.prototype   = new Person(); // *

const personJohn2    = new Person("John");
const sysAdmin2      = new Employee("Bob");
const clientNatalia2 = new Client("Natalia");

personJohn2.sayHello();
sysAdmin2.sayHello();
clientNatalia2.sayHello();


/**
 * Задача 3
 */
function Student(name, grades) {
    this.name        = name;
    this.grades      = grades;
    this.averageMark = () => {
        return (this.grades.reduce((sum, grade) => sum + grade) / this.grades.length).toFixed(1);
    }
    this.showInfo = () => {
        console.log(`Average mark of ${this.name} is: ${this.averageMark()}`);
    }
}

const students = [
    new Student('Student 1', [10,9,8,0,10]),
    new Student('Student 12', [10,0,8,0,3,4])
];

students[0].showInfo();
students[1].showInfo();
