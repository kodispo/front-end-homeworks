taskOne();
taskTwo();
taskThree();
taskFour();
taskFive();


/**
 * Найти дом узел в котором храниться год (2021) и поменять значение года на 2020.
 */
function taskOne() {
    document.body.innerHTML = document.body.innerHTML.replace(/2021/g, '2020');
}

/**
 * Найти дом узел (хтмл елемент) с аттрибутом my-attribute и удалить его (аттрибут из элемента).
 */
function taskTwo() {
    const element = document.querySelector('[my-attribute]');
    if (element) element.removeAttribute('my-attribute');
}

/**
 * Ячейки, у которых есть аттрибут data-id подсветить зеленым цветом
 */
function taskThree() {
    for (const element of document.querySelectorAll('[data-id]')) {
        element.style.backgroundColor = 'green';
    }
}

/**
 * Добавить строку в таблицу (в конец таблицы) с любыми значениями для Company, Contact и Revenue
 */
function taskFour() {
    const newRow  = document.createElement('tr');
    const company = document.createElement('td');
    const contact = document.createElement('td');
    const revenue = document.createElement('td');

    const companyText = document.createTextNode('McDonald\'s');
    const contactText = document.createTextNode('David Anderson');
    const revenueText = document.createTextNode('5B');

    company.append(companyText);
    contact.append(contactText);
    revenue.append(revenueText);
    revenue.setAttribute('data-id', 'revenue-value');
    newRow.append(company, contact, revenue);

    const rows  = document.querySelectorAll('table tbody tr');
    const total = rows[rows.length - 1];

    total.before(newRow);
}

/**
 * Удалить первую строку таблицы
 */
function taskFive() {
    document.querySelectorAll('table tbody tr')[1].remove();
}
