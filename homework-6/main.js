const items = [
    {
        title: "IPhone 12",
        quantity: 2,
        price: 1000,
    },
    {
        title: "Magic Mouse",
        quantity: 3,
        price: 100,
    },
    {
        title: "MI Band 6",
        quantity: 4,
        price: 50,
    },
    {
        title: "Monitor ASUS",
        quantity: 1,
        price: 700,
    },
    {
        title: "USB Flash Drive",
        quantity: 5,
        price: 20,
    }
];

printTotalAmount(items);
printAverageProductPrice(items);
printSortedProducts(items);



function printTotalAmount(products) {
    console.log(`Общая стоимость вашего заказа: $${countTotalAmount(products)}`);
}

function printAverageProductPrice(products) {
    console.log(`Cредняя цена 1 товара вашего заказа: $${countAverageProductPrice(products)}`);
}

function printSortedProducts(products) {
    console.table(sortProducts(products));
}

function countTotalAmount(products) {
    let totalAmount = 0;
    for (let i = 0; i < products.length; i++) {
        totalAmount += products[i].quantity * products[i].price;
    }
    return totalAmount;
}

function countTotalQuantity(products) {
    let totalQuantity = 0;
    for (let i = 0; i < products.length; i++) {
        totalQuantity += products[i].quantity;
    }
    return totalQuantity;
}

function countAverageProductPrice(products) {
    return countTotalAmount(products) / countTotalQuantity(products);
}

function sortProducts(products) {
    return products.sort(function(a, b) {
        return a.price - b.price;
    });
}