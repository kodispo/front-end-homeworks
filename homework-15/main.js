/**
 * Задание 1
 */
// 1
const user = {
    firstName: "Nikola",
    secondName: "Tesla",
    getFullName: function() {
        return `${this.firstName} ${this.secondName}`;
    }
}
console.log(user.getFullName());

// 2
const jobs = {
    firstName: "Steve",
    secondName: "Jobs",
}
console.log(user.getFullName.call(jobs));

// 3
const wozniak = {
    firstName: "Steve",
    secondName: "Wozniak",
}
console.log(user.getFullName.apply(wozniak));

// 4
const roberts = {
    firstName: "Julia",
    secondName: "Roberts",
}
const getFullName = user.getFullName.bind(roberts);
console.log(getFullName());

// 5
function User(firstName, secondName) {
    this.firstName   = firstName;
    this.secondName  = secondName;
    this.getFullName = () => `${this.firstName} ${this.secondName}`;
}
console.log(new User('David', 'Guetta').getFullName());
console.log(new User('Tom', 'Hanks').getFullName());
console.log(new User('Charlize', 'Theron').getFullName());


/**
 * Задание 2
 */
function Calculator(initial) {
    this.result = +initial;
    this.sum    = (number) => this.result += +number;
    this.mult   = (number) => this.result *= +number;
    this.sub    = (number) => this.result -= +number;
    this.div    = (number) => this.result /= +number;
    this.set    = (number) => this.result  = +number;
}

const calc = new Calculator(10);

console.log('calc.sum(5) =', calc.sum(5)); // 15
console.log('calc.mult(10) =', calc.mult(10)); // 150
console.log('calc.sub(40) =', calc.sub(40)); // 110
console.log('calc.div(10) =', calc.div(10)); // 11
console.log('calc.set(100) =', calc.set(100)); // 100