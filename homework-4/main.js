const radius = prompt('Введите радиус круга:');

if (!radius || isNaN(+radius)) {
    alert('Нужно ввеcти число');
} else {
    const operation = prompt('Введите номер команды чтобы посчитать:\n1 - диаметр, 2 - площадь круга, 3 - длинну окружности');
    switch (operation) {
        case '1': calcCircleDiameter(radius); break;
        case '2': calcCircleSquare(radius); break;
        case '3': calcCircumference(radius); break;
        default: alert('Нет такой команды');
    }
}

function calcCircleDiameter(radius) {
    const diameter = radius * 2;
    alert(`Диаметр окружности с радиусом ${radius} равен ${diameter}.`);
}

function calcCircleSquare(radius) {
    const square = Math.round(Math.PI * Math.pow(radius, 2));
    alert(`Площадь окружности с радиусом ${radius} равна ${square}.`);
}

function calcCircumference(radius) {
    const circumference = Math.round(2 * Math.PI * radius);
    alert(`Длина окружности с радиусом ${radius} равна ${circumference}.`);
}