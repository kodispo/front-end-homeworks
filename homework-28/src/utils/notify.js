import Noty from "noty";

export function notify(text, type = 'success') {
    new Noty({
        text: text,
        type: type,
        timeout: 2000,
    }).show();
}