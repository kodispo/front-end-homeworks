// styles
import "reset-css";
import "noty/src/noty.scss";
import "noty/src/themes/mint.scss";
import "./styles.scss";

import {User} from "./classes/User";
import {LoginForm} from "./classes/LoginForm";
import {Chat} from "./classes/Chat";

const loginForm = new LoginForm();

if (!User.isLoggedIn()) {
    loginForm.domEl.style.display = 'block';
} else {
    const chat = new Chat();
    loginForm.chatInstance = chat;
    chat.domEl.style.display = 'block';
}