import {notify} from "../utils/notify";
import {User} from "./User";
import {Chat} from "./Chat";

export class LoginForm {
    selector = '#login-form';
    chatInstance;

    constructor() {
        this.domEl = document.querySelector(this.selector);
        this.domEl.addEventListener('submit', this.handleSubmit.bind(this));
    }

    handleSubmit(e) {
        e.preventDefault();
        const inputEl = e.target.querySelector('[type="text"]');
        const username = inputEl.value;
        if (!User.validateUsername(username)) {
            return;
        }
        const user = new User(username);
        user.login();
        inputEl.value = '';
        notify('Successfully logged in.');
        this.domEl.style.display = 'none';
        if (!this.chatInstance) {
            this.chatInstance = new Chat();
        } else {
            this.chatInstance.showAvatar();
            this.chatInstance.createConnection();
        }
        this.chatInstance.domEl.style.display = 'block';
    }
}