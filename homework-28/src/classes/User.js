import {notify} from "../utils/notify";

export class User {
    constructor(username) {
        if (username) {
            this.name = username;
        }
    }

    login() {
        localStorage.setItem('username', this.name);
    }

    static validateUsername(username) {
        let isValid = true;
        if (!username) {
            notify('Username must not be empty.', 'error');
            isValid = false;
        }
        if (username.length <= 2) {
            notify('Username must be more than 2 characters long.', 'error');
            isValid = false;
        }
        return isValid;
    }

    static isLoggedIn() {
        return !!localStorage.getItem('username');
    }
}