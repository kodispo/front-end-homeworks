import {notify} from "../utils/notify";
import moment from "moment";

export class Chat {
    selector = '#chat-box';

    constructor() {
        this.domEl = document.querySelector(this.selector);
        this.loginFormEl = document.getElementById('login-form');
        this.reconnectEl = document.getElementById('reconnect');
        this.userListEl = document.querySelector('#people-list .list');
        this.chatEl = document.querySelector('.chat-history ul');
        this.logoutEl = document.getElementById('logout');
        this.messageFormEl = document.getElementById('chat-message');
        this.messageInputEl = document.getElementById('message-to-send');

        this.showAvatar();
        this.createConnection();

        this.logoutEl.addEventListener('click', this.handleLogoutClick.bind(this));
        this.reconnectEl.checked = !!localStorage.getItem('reconnect');
        this.reconnectEl.addEventListener('click', this.handleReconnectClick.bind(this));
        this.messageFormEl.addEventListener('submit', this.handleMessageFormSubmit.bind(this));
        this.messageInputEl.addEventListener('keydown', this.handleMessageInputElKeyDown.bind(this));

        this.incomingTemplate = document.getElementById('message-template').innerHTML;
        this.outgoingTemplate = document.getElementById('message-response-template').innerHTML;
        const storageMessages = this.getStorageMessages();
        if (storageMessages) {
            storageMessages.forEach((messageObj) => {
               const tmpl = messageObj.proper ? this.outgoingTemplate : this.incomingTemplate;
               this.showMessage(messageObj, tmpl);
            });
            setTimeout(this.scrollToTheLastMessage, 100);
        }

        const userList = this.getStorageUserList();
        if (userList) {
            userList.forEach((username) => {
                this.showUser(username);
            });
        }
    }

    handleLogoutClick(e) {
        e.preventDefault();
        this.ws.close();
        localStorage.clear();
        this.userListEl.innerHTML = '';
        this.chatEl.innerHTML = '';
        this.domEl.style.display = 'none';
        this.loginFormEl.style.display = 'block';
        notify('Logged out. Goodbye!');
    }

    handleReconnectClick(e) {
        if (e.target.checked) {
            localStorage.setItem('reconnect', 'true');
            if (this.ws.readyState === 3) {
                this.createConnection();
            }
        } else {
            localStorage.removeItem('reconnect');
        }
    }

    handleMessageInputElKeyDown(e) {
        if(e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            this.messageFormEl.querySelector('button').click();
        }
    }

    showAvatar() {
        const username = localStorage.getItem('username');
        const usernameEl = document.querySelector('#chat-username');
        const avatarEl = document.querySelector('.avatar');
        usernameEl.innerHTML = username;
        avatarEl.setAttribute('src', `https://eu.ui-avatars.com/api/?name=${encodeURIComponent(username)}&background=86BB71&color=fff`);
    }

    createConnection() {
        this.ws = new WebSocket(process.env.WEBSOCKET_URL);
        this.ws.onopen = () => {
            notify('Connected successfully');
            // setTimeout(function () {
            //     this.ws.close();
            // }.bind(this), 2000);
        };
        this.ws.onerror = () => {
            notify('Connection error!', 'error');
        };
        this.ws.onclose = () => {
            notify('Connection closed', 'warning');
            if (localStorage.getItem('reconnect')) {
                this.createConnection();
            }
        };
        this.ws.onmessage = (e) => {
            if (Chat.isJsonParsable(e.data)) {
                const parsedData = JSON.parse(e.data);
                if (parsedData.type !== 'message' || parsedData.payload.username === localStorage.getItem('username')) {
                    return;
                }
                const username = parsedData.payload.username;
                const messageText = parsedData.payload.message;
                const messageObj = this.getMessageObj(username, messageText);
                this.showMessage(messageObj, this.incomingTemplate);
                this.scrollToTheLastMessage();
                this.addMessageToStorage(messageObj);
                if (this.addUserToStorage(username)) {
                    this.showUser(username);
                }
            }
        };
    }

    handleMessageFormSubmit(e) {
        e.preventDefault();
        const username = localStorage.getItem('username');
        const messageText = this.messageInputEl.value;
        const messageObj = this.getMessageObj(username, messageText, true);
        if (this.sendMessage(messageObj)) {
            this.messageInputEl.value = '';
            this.showMessage(messageObj, this.outgoingTemplate);
            this.scrollToTheLastMessage();
            this.addMessageToStorage(messageObj);
        }
    }

    getMessageObj(username, messageText, proper = false) {
        return {
            username,
            messageText,
            proper,
            time: moment().calendar()
        }
    }

    sendMessage(messageObj) {
        const {messageText, username} = messageObj;
        if (messageText && this.ws.readyState === 1) {
            this.ws.send(JSON.stringify({
                type: 'message',
                payload: {
                    username: username,
                    message: messageText
                }
            }));
            return true;
        }
    }

    showMessage(messageObj, tmpl) {
        const {messageText, username, time} = messageObj;
        tmpl = tmpl.replace('{{username}}', username);
        tmpl = tmpl.replace('{{time}}', time);
        tmpl = tmpl.replace('{{messageText}}', messageText);
        this.chatEl.insertAdjacentHTML('beforeend', tmpl);
    }

    addMessageToStorage(messageObj) {
        let messages = this.getStorageMessages();
        messages.push(messageObj);
        localStorage.setItem('messages', JSON.stringify(messages));
    }

    getStorageMessages() {
        const messages = localStorage.getItem('messages');
        return (messages) ? JSON.parse(messages) : [];
    }

    getStorageUserList() {
        const userList = localStorage.getItem('userList');
        return (userList) ? JSON.parse(userList) : [];
    }

    addUserToStorage(username) {
        const userList = this.getStorageUserList();
        if (!userList.includes(username)) {
            userList.push(username);
            localStorage.setItem('userList', JSON.stringify(userList));
            return true;
        }
    }

    showUser(username) {
        let tmpl = document.getElementById('userlist-template').innerHTML;
        tmpl = tmpl.replace('{{username}}', username);
        tmpl = tmpl.replace('{{iconName}}', encodeURIComponent(username));
        this.userListEl.insertAdjacentHTML('beforeend', tmpl);
    }

    scrollToTheLastMessage() {
        const chatWrapEl = document.querySelector('.chat-history');
        chatWrapEl.scrollTop = chatWrapEl.scrollHeight;
    }

    static isJsonParsable(data) {
        try {
            JSON.parse(data);
        } catch (e) {
            return false;
        }
        return true;
    }
}