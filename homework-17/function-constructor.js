// sizes
Hamburger.SIZE_SMALL  = {price: 50, calories: 20}
Hamburger.SIZE_MEDIUM = {price: 75, calories: 30}
Hamburger.SIZE_LARGE  = {price: 100, calories: 40}

// toppings
Hamburger.TOPPING_CHEESE = {price: 10, calories: 20}
Hamburger.TOPPING_SALAD  = {price: 20, calories: 5}
Hamburger.TOPPING_POTATO = {price: 15, calories: 10}
Hamburger.TOPPING_SPICES = {price: 15, calories: 0}
Hamburger.TOPPING_MAYO   = {price: 20, calories: 5}

function Hamburger(size) {
    this.price      = size.price;
    this.calories   = size.calories;
    this.addTopping = (topping) => {
        this.price    += topping.price;
        this.calories += topping.calories;
    }
    this.getPrice    = () => this.price;
    this.getCalories = () => this.calories;
}


const hamburger = new Hamburger(Hamburger.SIZE_SMALL);

hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_POTATO);

console.log(`Total price: ${hamburger.getPrice()}`);
console.log(`Total calories: ${hamburger.getCalories()}`);