class Hamburger {
    // sizes
    static SIZE_SMALL  = {price: 50, calories: 20}
    static SIZE_MEDIUM = {price: 75, calories: 30}
    static SIZE_LARGE  = {price: 100, calories: 40}

    // toppings
    static TOPPING_CHEESE = {price: 10, calories: 20}
    static TOPPING_SALAD  = {price: 20, calories: 5}
    static TOPPING_POTATO = {price: 15, calories: 10}
    static TOPPING_SPICES = {price: 15, calories: 0}
    static TOPPING_MAYO   = {price: 20, calories: 5}

    constructor(size) {
        this.price    = size.price;
        this.calories = size.calories;
    }

    addTopping(topping) {
        this.price    += topping.price;
        this.calories += topping.calories;
    }

    getPrice() {
        return this.price;
    }

    getCalories() {
        return this.calories;
    }
}


const hamburger = new Hamburger(Hamburger.SIZE_SMALL);

hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_POTATO);

console.log(`Total price: ${hamburger.getPrice()}`);
console.log(`Total calories: ${hamburger.getCalories()}`);