window.addEventListener('load', handleWindowLoad);

function handleWindowLoad() {
    const removeAttrBtn   = document.getElementById('removeAttrBtn');
    const setGreenBtn     = document.getElementById('setGreenBtn');
    const addRowBtn       = document.getElementById('addRowBtn');
    const remove2ndRowBtn = document.getElementById('removeSecondRowBtn');

    removeAttrBtn.addEventListener('click', handleRemoveAttrBtnClick);
    setGreenBtn.addEventListener('click', handleSetGreenBtnClick, true);
    addRowBtn.addEventListener('click', handleAddRowBtnClick);
    remove2ndRowBtn.addEventListener('click', handleRemove2ndRowBtnClick);
}

function handleYearBtnClick() {
    const inputElem  = document.querySelector('input[name="year"]');
    const inoutValue = inputElem.value;

    if (!inoutValue) return;

    const yearElem = document.querySelector('#title span');
    yearElem.textContent = inoutValue;
    inputElem.value = '';
}

function handleRemoveAttrBtnClick() {
    const elem = document.querySelector('[my-attribute]');
    if (elem) elem.removeAttribute('my-attribute');
}

function handleSetGreenBtnClick(e) {
    e.stopPropagation();
    for (const element of document.querySelectorAll('[data-id]')) {
        element.style.backgroundColor = 'green';
    }
}

function handleAddRowBtnClick() {
    const newRow  = document.createElement('tr');
    const company = document.createElement('td');
    const contact = document.createElement('td');
    const revenue = document.createElement('td');

    const companyText = document.createTextNode('McDonald\'s');
    const contactText = document.createTextNode('David Anderson');
    const revenueText = document.createTextNode('5B');

    company.append(companyText);
    contact.append(contactText);
    revenue.append(revenueText);
    revenue.setAttribute('data-id', 'revenue-value');
    newRow.append(company, contact, revenue);

    const rows  = document.querySelectorAll('table tbody tr');
    const total = rows[rows.length - 1];

    total.before(newRow);
}

function handleRemove2ndRowBtnClick() {
    const elem = document.querySelectorAll('table tbody tr')[1];
    if (elem) elem.remove();
}

// because of async
if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', handleDOMContentLoaded);
} else {
    handleDOMContentLoaded();
}

function handleDOMContentLoaded() {
    const titleElem = document.getElementById('title');
    titleElem.style.fontSize = '24px';
}