const questions = {
    1: {
        question: 'Сколько будет 2 + \'2\' в JavaScript?',
        answer: '22'
    },
    2: {
        question: 'Назовите оператор присваивания в JavaScript',
        answer: '='
    },
    3: {
        question: 'Сколько будет 5 / 0?',
        answer: 'Infinity'
    },
    4: {
        question: 'Одинаково ли работает == и === в JavaScript?',
        answer: 'no'
    },
    5: {
        question: 'Чему равно выражение Boolean(null) в JavaScript?',
        answer: 'false'
    }
}
const questionsCount = Object.keys(questions).length;
const scoreIncrement = 10;
const passPercentage = 50;
const scoreToPass    = questionsCount * scoreIncrement * passPercentage / 100;

let score = 0;

for (const question in questions) {
    if (prompt(questions[question].question) === questions[question].answer) {
        score += 10;
    }
}

let summary = (score > scoreToPass) ? 'Ура! Вы успешно прошли тест!' : 'К сожалению тест провален.';

alert(`Ваш результат: ${score} баллов. ${summary}`);