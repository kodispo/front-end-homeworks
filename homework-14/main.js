/**
 * Задача 1
 */
function arrayFillViaForLoop(value, count) {
    let array = [];
    for (; count > 0; count--) {
        array.push(value);
    }
    return array;
}

function arrayFillViaFillMethod(value, count) {
    return new Array(count).fill(value, 0, count);
}

console.log('arrayFillViaForLoop(\'x\', 5)', arrayFillViaForLoop('x', 5));
console.log('arrayFillViaFillMethod(\'x\', 5)', arrayFillViaFillMethod('x', 5));


/**
 * Задача 2
 */
function sumArrayValuesViaForLoop(array) {
    let result = 0;
    for (let i = 0; i < array.length; i++) {
        result += (Array.isArray(array[i])) ? sumArrayValuesViaForLoop(array[i]) : array[i];
    }
    return result;
}

function sumArrayValuesViaFlatAndReduce(array) {
    return array.flat(Infinity).reduce((sum, el) => sum + el);
}

const array = [[1, 2, 3], [4, 5], [6]];
console.log('sumArrayValuesViaForLoop', sumArrayValuesViaForLoop(array));
console.log('sumArrayValuesViaFlatAndReduce', sumArrayValuesViaFlatAndReduce(array));


/**
 * Задача 3
 */
const countersArr    = [];
const countersFormEl = document.getElementById('counters-form');
const countersListEl = document.getElementById('counters');

countersFormEl.addEventListener('submit', handleCountersFormSubmit);
countersListEl.addEventListener('click', handleCounterClick);
countersListEl.addEventListener('click', handleRemoveCounterClick);

function handleCountersFormSubmit(e) {
    e.preventDefault();

    const inputEl = document.getElementById('new-counter-initial-number');
    const initNum = inputEl.value;
    if (!initNum) return;

    const counter = new Counter(initNum, countersArr.length);
    countersArr.push(counter);

    const counterEl = counter.createCounterEl();
    countersListEl.append(counterEl);

    inputEl.value = 0;
    inputEl.focus();
}

function handleCounterClick(e) {
    const el = e.target;
    if (el.closest('.counter') && !el.classList.contains('close')) {
        const counterEl = el.closest('.counter');
        const counterId = counterEl.dataset.id;
        const counter   = countersArr[counterId];
        counterEl.querySelector('.badge').textContent = counter.count();
    }
}

function handleRemoveCounterClick(e) {
    const el = e.target;
    if (el.classList.contains('close')) {
        const counterEl = el.closest('.counter');
        const counterId = counterEl.dataset.id;
        countersArr[counterId] = null;
        counterEl.remove();
    }
}

function Counter(initNum = 0, id) {
    let counterValue = initNum;

    this.id = id;

    this.count = () => ++counterValue;

    this.getCounterHtml = () => {
        return `
            <div data-id="${this.id}" class="counter d-flex align-items-center bg-white rounded p-2 mb-2">
                <button type="button" class="btn btn-primary w-100">
                  Count <span class="badge bg-warning text-dark">${initNum}</span>
                </button>
                <button class="close text-danger ml-3" type="button">×</button>
            </div>    
        `;
    };

    this.convertHtmlToEl = (html) => {
        let template = document.createElement('template');
        template.innerHTML = html.trim();
        return template.content.firstChild;
    };

    this.createCounterEl = () => {
        const html = this.getCounterHtml();
        return this.convertHtmlToEl(html);
    }
}

