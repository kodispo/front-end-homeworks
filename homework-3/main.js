const operations   = ['+', 'add', '-', 'sub', '*', 'mult', '/', 'div', '%', 'mod'];
const errorMessage = 'Validation error. Please try again.';
const number1      = getNumber('Please specify the 1st number');
const number2      = getNumber('Please specify the 2nd number');
const operation    = getOperation(`Please specify which operation to perform: \n${operations.join(', ')}.`);

alert(getResult(number1, number2, operation));


function getNumber(message) {
    let number = prompt(message);
    if (!number || isNaN(+number)) {
        alert(errorMessage);
        number = getNumber(message);
    }
    return +number;
}

function getOperation(message) {
    let operation = prompt(message);
    if (!operations.includes(operation)) {
        alert(errorMessage);
        operation = getOperation(message);
    }
    return operation;
}

function getResult(firstNumber, secondNumber, operation) {
    switch (operation) {
        case '+':
        case 'add':
            return `${firstNumber} + ${secondNumber} = ${firstNumber + secondNumber}`;

        case '-':
        case 'sub':
            return `${firstNumber} - ${secondNumber} = ${firstNumber - secondNumber}`;

        case '*':
        case 'mult':
            return `${firstNumber} * ${secondNumber} = ${firstNumber * secondNumber}`;

        case '/':
        case 'div':
            return `${firstNumber} / ${secondNumber} = ${firstNumber / secondNumber}`;

        case '%':
        case 'mod':
            return `${firstNumber} % ${secondNumber} = ${firstNumber % secondNumber}`;
    }
}